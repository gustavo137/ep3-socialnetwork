# README

To run the application it is necessary to have the ruby and gem rails installed, the database is managed by PostGreeSQL,
to run first give the command "bundle install" then the command "db: migrate" and then if you want the populate database 
with self-generated information execute the "db: seed" command, run the server using "rails s", the project will be 
available in the localhost of the browser, usually : http://localhost:3000/ .

* Ruby version: ruby 2.5.1p57

* Rails version:  5.2.1

* Database creation: db: create, using postgree it is necessary to have a database superuser.

Authored-by: Gustavo Afonso <gugustavo137@gmail.com>

Authored-by: Gabrielle Ribeiro <gabrielleribeiro2010@gmail.com>"