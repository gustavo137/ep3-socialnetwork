class RegistrationsController < Devise::RegistrationsController

	private

  	def signUpParameters
    	parameters.require(:user).permit(:name,
				      	 :nickname, 
                                         :email, 
                                         :password, 
                                         :password_confirmation)
  	end

  	def accountUpdateParameters
    	parameters.require(:user).permit(:name,
					 :nickname, 
                                  	 :email, 
                                  	 :password, 
                                  	 :password_confirmation, 
                                  	 :current_password)
  	end
end
